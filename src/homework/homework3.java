package homework;

public class homework3 {
    public static void main(String[] args){
        System.out.println("draw9(2)");
        draw9(2);
        System.out.println("draw9(3)");
        draw9(3);
        System.out.println("draw9(4)");
        draw9(4);
        System.out.println("draw10(2)");
        draw10(2);
        System.out.println("draw10(3)");
        draw10(3);
        System.out.println("draw10(4)");
        draw10(4);
        System.out.println("draw11(2)");
        draw11(2);
        System.out.println("draw11(3)");
        draw11(3);
        System.out.println("draw11(4)");
        draw11(4);
        System.out.println("draw12(2)");
        draw12(2);
        System.out.println("draw12(3)");
        draw12(3);
        System.out.println("draw12(4)");
        draw12(4);
        System.out.println("draw13(2)");
        draw13(2);
        System.out.println("draw13(3)");
        draw13(3);
        System.out.println("draw13(4)");
        draw13(4);
        System.out.println("draw14(2)");
        draw14(2);
        System.out.println("draw14(3)");
        draw14(3);
        System.out.println("draw14(4)");
        draw14(4);
        System.out.println("draw15(2)");
        draw15(2);
        System.out.println("draw15(3)");
        draw15(3);
        System.out.println("draw15(4)");
        draw15(4);
        System.out.println("draw16(2)");
        draw16(2);
        System.out.println("draw16(3)");
        draw16(3);
        System.out.println("draw16(4)");
        draw16(4);
        System.out.println("draw17(2)");
        draw17(2);
        System.out.println("draw17(3)");
        draw17(3);
        System.out.println("draw17(4)");
        draw17(4);
    }
    //3.1
    public static void draw9(int n){
        for (int index = 0; index < n; index++){
            System.out.println(index * 2);
        }
    }
    //3.2
    public static void draw10(int n){
        for (int index = 1; index <= n; index++){
            System.out.println(index * 2);
        }
    }
    //3.3
    public static void draw11(int n){
        for (int index = 1; index <= n; index++){
            int start = index;
            int count = 1;
            for (int sindex = 1; sindex <= n; sindex++){
                System.out.print(start*count);
                count++;
            }
            System.out.println("");
        }
    }
    //3.4
    public static void draw12(int n){
        int count = 1;
        for (int index = 1; index <= n; index++){
            for (int sindex = 1; sindex <= n; sindex++){
                if (count == sindex){
                    System.out.print("-");
                }else{
                    System.out.print("*");
                }
            }
            count++;
            System.out.println("");
        }
    }
    //3.5
    public static void draw13(int n){
        int count = n;
        for (int index = 1; index <= n; index++){
            for (int sindex = 1; sindex <= n; sindex++){
                if (count == sindex){
                    System.out.print("-");
                }else{
                    System.out.print("*");
                }
            }
            count--;
            System.out.println("");
        }
    }
    //3.6
    public static void draw14(int n){
        for (int index = n; index > 0; index--){
            for (int sindex = n; sindex >= 1; sindex--){
                if (index > sindex){
                    System.out.print("-");
                }else{
                    System.out.print("*");
                }
            }
            System.out.println("");
        }
    }
    //3.7
    public static void draw15(int n){
        for (int index = 0; index < n; index++){
            for (int sindex = n; sindex > 0; sindex--){
                if (index < sindex){
                    System.out.print("*");
                }else{
                    System.out.print("-");
                }
            }
            System.out.println("");
        }
    }
    //3.8
    public static void draw16(int n){
        for (int index = 0; index < n; index++){
            for (int sindex = 0; sindex < n; sindex++){
                if (sindex > index){
                    System.out.print("-");
                }else{
                    System.out.print("*");
                }
            }
            System.out.println("");
        }
        for (int index = n; index > 1; index--){
            for (int sindex = 0; sindex < n; sindex++){
                if (sindex < index - 1){
                    System.out.print("*");
                }else{
                    System.out.print("-");
                }
            }
            System.out.println("");
        }
    }
    //3.9
    public static void draw17(int n){
        for (int index = 1; index < n + 1; index++){
            for (int sindex = 1; sindex < n + 1; sindex++){
                if (sindex > index){
                    System.out.print("-");
                }else{
                    System.out.print(index);
                }
            }
            System.out.println("");
        }
        for (int index = n; index > 1; index--){
            for (int sindex = 0; sindex < n; sindex++){
                if (sindex < index - 1){
                    System.out.print(index - 1);
                }else{
                    System.out.print("-");
                }
            }
            System.out.println("");
        }
    }
}
