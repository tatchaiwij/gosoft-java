package homework;

public class homework2 {
    public static void main(String[] args){
        String[][] table = {
            { "1", "2", "3" },
            { "4", "5", "6" },
            { "7", "8", "9" }
        };
        multiplyTable(table);
    }
    public static void multiplyTable(String[][] input){
        for(String[] row : input){
            for(int coloum = 0; coloum < row.length; coloum++){
                System.out.print(Integer.parseInt(row[coloum]) * 2);
                if(coloum < row.length - 1){
                    System.out.print(", ");
                }
            }
            System.out.println("");
        }
    }
}