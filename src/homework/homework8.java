package homework;

public class homework8 {
    public static void main(String[] args){
        System.out.println("draw1(2)");
        draw1(2);
        System.out.println("draw1(3)");
        draw1(3);
        System.out.println("draw1(4)");
        draw1(4);    
        System.out.println("draw2(2)");
        draw2(2);
        System.out.println("draw2(3)");
        draw2(3);
        System.out.println("draw2(4)");
        draw2(4);  
        System.out.println("draw3(2)");
        draw3(2);
        System.out.println("draw3(3)");
        draw3(3);
        System.out.println("draw3(4)");
        draw3(4);  
        System.out.println("draw4(2)");
        draw4(2);
        System.out.println("draw4(3)");
        draw4(3);
        System.out.println("draw4(4)");
        draw4(4);  
        System.out.println("draw5(2)");
        draw5(2);
        System.out.println("draw5(3)");
        draw5(3);
        System.out.println("draw5(4)");
        draw5(4);  
        System.out.println("draw6(2)");
        draw6(2);
        System.out.println("draw6(3)");
        draw6(3);
        System.out.println("draw6(4)");
        draw6(4);  
        System.out.println("draw7(2)");
        draw7(2);
        System.out.println("draw7(3)");
        draw7(3);
        System.out.println("draw7(4)");
        draw7(4);  
        System.out.println("draw8(2)");
        draw8(2);
        System.out.println("draw8(3)");
        draw8(3);
        System.out.println("draw8(4)");
        draw8(4);      
    }
    //1.1
    public static void draw1(int n){
        for(int index = 0; index < n; index++){
            System.out.print("*");
        }
        System.out.println("");
    }
    //1.2
    public static void draw2(int n){
        for(int index = 0; index < n; index++){
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print("*");
            }
        System.out.println("");
        }
    }
    //1.3
    public static void draw3(int n){
        for(int index = 0; index < n; index++){
            int count = 1;
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print(count);
                count++;
            }
        System.out.println("");
        }
    }
    //1.4
    public static void draw4(int n){
        for(int index = 0; index < n; index++){
            int count = n;
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print(count);
                count--;
            }
        System.out.println("");
        }
    }
    //1.5
    public static void draw5(int n){
        int count = 1;
        for(int index = 0; index < n; index++){
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print(count);
            }
            count++;
        System.out.println("");
        }
    }
    //1.6
    public static void draw6(int n){
        int count = n;
        for(int index = 0; index < n; index++){
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print(count);
            }
            count--;
        System.out.println("");
        }
    }
    //1.7
    public static void draw7(int n){
        int count = 1;
        int padding = Integer.toString(n*n).length();
        for(int index = 0; index < n; index++){
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print(count);
                int lenght = Integer.toString(count).length();
                for(int paddingindex = 0; paddingindex < padding - lenght; paddingindex++){
                    System.out.print(" ");
                }
                count++;
            }
        System.out.println("");
        }
    }
    //1.8
    public static void draw8(int n){
        int count = n*n;
        int padding = Integer.toString(count).length();
        for(int index = 0; index < n; index++){
            for(int printindex = 0; printindex < n; printindex++){
                System.out.print(count);
                int lenght = Integer.toString(count).length();
                for(int paddingindex = 0; paddingindex < padding - lenght; paddingindex++){
                    System.out.print(" ");
                }
                count--;
            }
        System.out.println("");
        }
    }
}
