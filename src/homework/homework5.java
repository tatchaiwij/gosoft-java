package homework;

public class homework5 {
    public static void main(String[] args){
        System.out.println("draw18(2)");
        draw18(2);
        System.out.println("draw18(3)");
        draw18(3);
        System.out.println("draw18(4)");
        draw18(4);
        System.out.println("draw19(2)");
        draw19(2);
        System.out.println("draw19(3)");
        draw19(3);
        System.out.println("draw19(4)");
        draw19(4);
        System.out.println("draw20(2)");
        draw20(2);
        System.out.println("draw20(3)");
        draw20(3);
        System.out.println("draw20(4)");
        draw20(4);
        System.out.println("draw21(2)");
        draw21(2);
        System.out.println("draw21(3)");
        draw21(3);
        System.out.println("draw21(4)");
        draw21(4);
        System.out.println("draw22(2)");
        draw22(2);
        System.out.println("draw22(3)");
        draw22(3);
        System.out.println("draw22(4)");
        draw22(4);
        System.out.println("draw23(2)");
        draw23(2);
        System.out.println("draw23(3)");
        draw23(3);
        System.out.println("draw23(4)");
        draw23(4);
        System.out.println("draw24(2)");
        draw24(2);
        System.out.println("draw24(3)");
        draw24(3);
        System.out.println("draw24(4)");
        draw24(4);
        System.out.println("draw25(2)");
        draw25(2);
        System.out.println("draw25(3)");
        draw25(3);
        System.out.println("draw25(4)");
        draw25(4);
    }
    //5.1
    public static void draw18(int n){
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index - 1){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }
            System.out.println(out);
        }
    }
    //5.2
    public static void draw19(int n){
        for (int index = 0; index < n; index++){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }
            System.out.println(out);
        }
    }
    //5.3
    public static void draw20(int n){
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index - 1){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }
            System.out.println(out);
        }
        for (int index = 0; index < n - 1; index++){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex > index){
                    out = out + "*";
                }else{
                    out = out + "-";
                }
            }
            System.out.println(out);
        }
    }
    //5.4
    public static void draw21(int n){
        int count = 1;
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index - 1){
                    out = out + "-";
                }else{
                    out = out + count;
                    count++;
                }
            }
            System.out.println(out);
        }
        for (int index = 0; index < n - 1; index++){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex > index){
                    out = out + count;
                    count++;
                }else{
                    out = out + "-";
                }
            }
            System.out.println(out);
        }
    }
    //5.5
    public static void draw22(int n){
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index - 1){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }
            for (int printindex = n; printindex > 1; printindex--){
                if (printindex > index){
                    out = out + "*";
                }else{
                    out = out + "-";
                }
            }
            System.out.println(out);
        }
    }
    //5.6
    public static void draw23(int n){
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = n; printindex > 0; printindex--){
                if (printindex > index){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }
            for (int printindex = 1; printindex < n; printindex++){
                if (printindex < index){
                    out = out + "*";
                }else{
                    out = out + "-";
                }
            }
            System.out.println(out);
        }
    }
    //5.7
    public static void draw24(int n){
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index - 1){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }
            for (int printindex = n; printindex > 1; printindex--){
                if (printindex > index){
                    out = out + "*";
                }else{
                    out = out + "-";
                }
            }
            System.out.println(out);
        }
        for (int index = 0; index < n - 1; index++){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex > index){
                    out = out + "*";
                }else{
                    out = out + "-";
                }
            }
            for (int printindex = n; printindex > 1; printindex--){
                if (printindex <= index + 2){
                    out = out + "-";
                }else{
                    out = out + "*";
                }
            }            
            System.out.println(out);
        }
    }
    //5.8
    public static void draw25(int n){
        int count = 1;
        for (int index = n; index > 0; index--){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex < index - 1){
                    out = out + "-";
                }else{
                    out = out + count;
                    count++;
                }
            }
            for (int printindex = n; printindex > 1; printindex--){
                if (printindex > index){
                    out = out + count;
                    count++;
                }else{
                    out = out + "-";
                }
            }
            System.out.println(out);
        }
        for (int index = 0; index < n - 1; index++){
            String out = "";
            for (int printindex = 0; printindex < n; printindex++){
                if (printindex > index){
                    out = out + count;
                    count++;
                }else{
                    out = out + "-";
                }
            }
            for (int printindex = n; printindex > 1; printindex--){
                if (printindex <= index + 2){
                    out = out + "-";
                }else{
                    out = out + count;
                    count++;
                }
            }            
            System.out.println(out);
        }
    }
}
